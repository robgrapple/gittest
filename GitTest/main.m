//
//  main.m
//  GitTest
//
//  Created by Robert Baker on 7/3/14.
//  Copyright (c) 2014 Robert Baker. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MCAppDelegate class]));
    }
}
