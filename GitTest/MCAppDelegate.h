//
//  MCAppDelegate.h
//  GitTest
//
//  Created by Robert Baker on 7/3/14.
//  Copyright (c) 2014 Robert Baker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
